const osc = require('osc');
const config = require('config');
const log = require('./log');
const PlayerAgent = require('./playerAgent');
const RelayAgent = require('./relayAgent');
const OscClient = require('./oscClient');

process.chdir(__dirname);

log.initializeLogging(config.get('logging'));

const networkConfig = config.get('network');
const multicastAddress = config.get('multicastAddress');

const oscPortConfig = {
  metadata: true,
  localAddress: networkConfig.localAddress,
  localPort: networkConfig.localPort || networkConfig.multicastPort,
  multicastTTL: networkConfig.multicastTTL,
};
if (config.actAsRelay) {
  oscPortConfig.remoteAddress = multicastAddress;
  oscPortConfig.remotePort = networkConfig.multicastPort;
} else {
  oscPortConfig.multicastMembership = multicastAddress;
}

const oscPort = new osc[config.get('oscPortType')](oscPortConfig);

let agent;
if (config.actAsRelay) {
  agent = new RelayAgent(oscPort, config.get('oscMessages'), config.get('relayInterval'));
} else {
  agent = new PlayerAgent(config.get('videoFiles'));
}

const oscClient = new OscClient(oscPort, config.get('oscMessages'));

oscPort.open();

oscPort.on("ready", () => {
  log.debug('OSC port open');
});

oscClient.on('open', file => agent.open(file));
oscClient.on('play', () => { agent.play(); });
oscClient.on('stop', () => { agent.stop(); });
