const log = require('./log');

module.exports = class RelayAgent extends Object {
  constructor(oscPort, oscMessagesConfig, relayInterval) {
    super();
    this.oscPort = oscPort;
    this.oscMessagesConfig = oscMessagesConfig;

    this.currentFile_ = null;
    this.playing_ = false;

    this.interval_ = setInterval(this.broadcast_.bind(this), relayInterval);
  }

  open(file) {
    this.currentFile_ = file;
  }

  play() {
    this.playing_ = true;
  }

  stop() {
    this.playing_ = false;
  }

  broadcast_() {
    if (!this.currentFile_) {
      log.debugIf('relay', 'No file seen, not broadcasting yet');
      return;
    }

    this.sendMessage_('marker', [{
      type: 's',
      value: this.oscMessagesConfig.marker.projectPrefix + this.currentFile_,
    }]);

    this.sendMessage_(this.playing_ ? 'play' : 'stop', [{
      type: 'f',
      value: 1,
    }]);
  }

  sendMessage_(messageName, args) {
    const address = this.oscMessagesConfig[messageName].addresses[0];
    log.debugIf('relay', 'Sending', address, args);
    this.oscPort.send({
      address,
      args,
    });
  }
}
