let loggingConfig;

module.exports = {
  initializeLogging: (config) => {
    loggingConfig = config;
  },

  debug: (...args) => {
    if (loggingConfig.enabled) {
      console.log(...args);
    }
  },

  debugIf: (enabled, ...args) => {
    if (loggingConfig[enabled]) {
      module.exports.debug(...args);
    }
  },
}
