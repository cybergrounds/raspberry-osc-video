const log = require('./log');
const EventEmitter = require('events');

module.exports = class OscClient extends EventEmitter {
  constructor(oscPort, oscMessagesConfig) {
    super();
    this.oscPort = oscPort;
    this.oscMessagesConfig = oscMessagesConfig;

    this.handlers_ = {};
    this.configureHandler_('play', this.onPlay_);
    this.configureHandler_('stop', this.onStop_);
    this.configureHandler_('pause', this.onPause_);
    this.configureHandler_('marker', this.onMarker_);

    this.markerRegexp_ = new RegExp('^' + this.oscMessagesConfig.marker.projectPrefix + '(\\w+)$');

    this.currentFile_ = undefined;
    this.playing_ = false;

    this.oscPort.on("message", (oscMsg, timeTag, info) => {
      log.debugIf('allMessages', "An OSC message just arrived!", oscMsg);
      const address = oscMsg.address;
      const handler = this.handlers_[address];
      if (!handler) {
        log.debugIf('unhandledMessages', 'Unhandled address:', address);
        return;
      }

      const args = oscMsg.args;
      handler(args);
    });
  }

  onPlay_(args) {
    if (!this.isToggleOn_(args)) return;
    if (!this.hasFile) {
      log.debug('File not loaded, play ignored');
      return;
    }

    if (this.playing) {
      log.debugIf('duplicateMessages', 'Toggle play ignored, already playing');
      return;
    }

    log.debug('Play!');
    this.playing_ = true;

    this.emit('play');
  }

  onPause_(args) {
    if (!this.isToggleOn_(args)) return;
    if (!this.hasFile) {
      log.debug('File not loaded, pause ignored');
      return;
    }

    log.debug('Pause ignored!');
  }

  onStop_(args) {
    if (!this.isToggleOn_(args)) return;
    if (!this.hasFile) {
      log.debug('File not loaded, stop ignored');
      return;
    }

    if (!this.playing) {
      log.debugIf('duplicateMessages', 'Toggle stop ignored, already stopped');
      return;
    }

    log.debug('Stop!');
    this.playing_ = false;

    this.emit('stop');
  }

  onMarker_(args) {
    if (!args) {
      log.debug('onMarker: No args received!');
      return;
    }
    if (!args.length) {
      log.debug('onMarker: Zero args received!');
      return;
    }

    const firstArg = args[0];
    if (firstArg.type !== 's') {
      log.debug('onMarker: First arg is not a string!');
      return;
    }

    const marker = firstArg.value;
    const markerMatch = this.markerRegexp_.exec(marker);
    if (!markerMatch) {
      log.debug('onMarker: Maker name does not match pattern!', marker);
      return;
    }

    const file = markerMatch[1];
    if (file === this.currentFile) {
      log.debugIf('duplicateMessages', 'File already open:', file);
      return;
    }

    log.debug('Open', file);
    this.currentFile_ = file;

    this.emit('open', file);
  }

  get currentFile() {
    return this.currentFile_;
  }

  get hasFile() {
    return this.currentFile !== undefined;
  }

  get playing() {
    return this.playing_;
  }

  isToggleOn_(args) {
    return args && args.length && args[0].value;
  }

  configureHandler_(name, handler) {
    this.oscMessagesConfig[name].addresses.forEach(address => {
      this.handlers_[address] = handler.bind(this);
    });
  }
}
