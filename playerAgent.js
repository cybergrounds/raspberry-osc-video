const child_process = require('child_process');
const fs = require('fs');
const path = require('path');
const log = require('./log');

const VLC_PARAMS = ['--intf', 'rc', '--start-paused', '--no-osd', '--fullscreen'];

module.exports = class PlayerAgent extends Object {
  constructor(videoFilesConfig) {
    super();
    this.videoFilesConfig = videoFilesConfig;
    this.currentChild_ = null;
  }

  open(file) {
    this.stopChild_().then(() => this.runChild_(file));
  }

  play() {
    this.sendCommands_(['play']);
  }

  stop() {
    this.sendCommands_(['pause', 'seek 0']);
  }

  runChild_(file) {
    const fullFileName = path.join(
      this.videoFilesConfig.path,
      this.videoFilesConfig.prefix + file + this.videoFilesConfig.extension
    );
    if (!fs.existsSync(fullFileName)) {
      log.debug('Project', file, 'not found!');
      return;
    }

    log.debug('Run VLC', fullFileName);
    this.currentChild_ = child_process.spawn('vlc', [...VLC_PARAMS, fullFileName]);
    this.currentChild_.stdout.on('data', () => {});
    this.currentChild_.stderr.on('data', () => {});
    this.currentChild_.stdin.setEncoding('utf-8');
    this.currentChild_.on('close', code => {
      log.debug('VLC stdio closed, code', code);
      this.currentChild_ = null;
    });
  }

  stopChild_(file) {
    if (this.currentChild_) {
      return new Promise(resolve => {
        this.currentChild_.removeAllListeners('close');
        this.currentChild_.on('exit', code => {
          log.debug('VLC exited, code', code)
          this.currentChild_ = null;
          resolve();
        });
        this.sendCommands_(['quit']);
      });
    } else {
      return Promise.resolve();
    }
  }

  sendCommands_(commands) {
    if (this.currentChild_) {
      commands.forEach(command => {
        this.currentChild_.stdin.write(command + '\n');
      });
    } else {
      log.debug('No file loaded');
    }
  }
}
